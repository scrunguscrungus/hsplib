﻿using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HSPLib
{
    public struct HSPFile
    {
        public string pageTitle; //data[0][1][1]
        public string pageAuthor; //data[0][1][2]

        public int unknown1; //data[0][1][3]

        public string pageMusic; //data[0][1][4]
        public string pageBG; //data[0][1][5]

        public int unknown2; //data[0][1][6]
        public int unknown3; //data[0][1][7]

        public string pageDesc; //data[0][1][8]
    }

    public class HSPReader
    {
        public static HSPFile ParseHSP(string path)
        {
            if (!File.Exists(path))
            {
                throw new IOException("ParseHSP(): File does not exist");
            }


            using (StreamReader reader = File.OpenText(path))
            {
                try
                {
                    JObject o = (JObject)JToken.ReadFrom(new JsonTextReader(reader));

                    HSPFile parsedData = new HSPFile();

                    parsedData.pageTitle = (string) o["data"][0][1][1];
                    parsedData.pageAuthor = (string) o["data"][0][1][2];

                    parsedData.unknown1 = (int) o["data"][0][1][3];

                    parsedData.pageMusic = (string) o["data"][0][1][4];
                    parsedData.pageBG = (string)o["data"][0][1][5];

                    parsedData.unknown2 = (int)o["data"][0][1][6];
                    parsedData.unknown3 = (int)o["data"][0][1][7];

                    parsedData.pageDesc = (string)o["data"][0][1][8];

                    return parsedData;

                }
                catch (Exception e)
                {
                    throw new ArgumentException(string.Format("ParseHSP(): The file was not a valid HSP file ({0})", e.Message));
                }
                
            }
        }
    }
}
